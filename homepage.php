<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<main class="site-content">

		<section class="hero">
			<div class="wrapper">

				<?php if(get_field('hero_headline')): ?>
					<div class="headline">
						<h1><?php the_field('hero_headline'); ?></h1>
					</div>
				<?php endif; ?>

				<?php if(get_field('hero_copy')): ?>
					<div class="copy p1">
						<?php the_field('hero_copy'); ?>
					</div>
				<?php endif; ?>
				
			</div>
		</section>

		<section class="restaurants">

			<?php if(have_rows('restaurants')): while(have_rows('restaurants')) : the_row(); ?>

			    <?php if( get_row_layout() == 'restaurant' ): ?>

			    	<?php get_template_part('partials/home/restaurant'); ?>
					
			    <?php endif; ?>

			<?php endwhile; endif; ?>	

		</section>
	
	</main>

<?php get_footer(); ?>