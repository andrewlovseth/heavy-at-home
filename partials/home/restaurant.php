<article class="restaurant" id="<?php echo sanitize_title_with_dashes(get_sub_field('name')); ?>">
	<div class="wrapper">		
		<div class="grid">
			
			<div class="photo">
				<div class="photo-wrapper">
					<div class="content">
						<?php if(get_sub_field('photo')): ?>
							<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						<?php endif; ?>
					</div>
				</div>

			</div>

			<div class="info">
				<?php if(get_sub_field('logo')): ?>
					<div class="headline">

						<div class="logo">
							<img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php the_sub_field('name'); ?>" />
						</div>
					</div>
				<?php endif; ?>

				<?php if(get_sub_field('copy')): ?>
					<div class="copy p2">
						<p><?php the_sub_field('copy'); ?></p>
					</div>
				<?php endif; ?>

				<div class="columns">
					<div class="col">

						<?php if(get_sub_field('website')): ?>
							<div class="dine-in module">
								<h3 class="module-header">Dine In</h3>
								<p><a href="<?php the_sub_field('website'); ?>" rel="external">Website</a></p>
							</div>
						<?php endif; ?>

						<?php if(get_sub_field('address')): ?>
							<div class="address module">
								<h3 class="module-header">Address</h3>
								<p><?php the_sub_field('address'); ?></p>
							</div>
						<?php endif; ?>

						<?php if(get_sub_field('phone')): ?>
							<div class="phone module">
								<h3 class="module-header">Phone</h3>
								<p><?php the_sub_field('phone'); ?></p>
							</div>
						<?php endif; ?>

						<?php if(have_rows('social')): ?>
							<div class="social module">
								<h3 class="module-header">Social</h3>

								<div class="social-links">
									<?php while(have_rows('social')): the_row(); ?>
										<a href="<?php the_sub_field('link'); ?>" rel="external">
											<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
										</a>
									<?php endwhile; ?>
								</div>
							</div>
						<?php endif; ?>				
					</div>

					<div class="col">
						<?php if(get_sub_field('menu_link')): ?>
							<div class="menu module">
								<h3 class="module-header">Menu</h3>
								<p><a href="<?php the_sub_field('menu_link'); ?>" rel="external">View menu</a></p>
							</div>
						<?php endif; ?>

						<?php if(have_rows('hours')): ?>
							<div class="hours module">
								<h3 class="module-header">Hours</h3>

								<?php while(have_rows('hours')): the_row(); ?>
									<div class="entry">
										<div class="day">
											<h5><?php the_sub_field('day'); ?></h5>
										</div>

										<div class="details">
											<p><?php the_sub_field('details'); ?></p>
										</div>
									</div>
								<?php endwhile; ?>

								<?php if(get_sub_field('hours_note')): ?>
									<div class="note">
										<em><?php the_sub_field('hours_note'); ?></em>
									</div>
								<?php endif; ?>
							</div>
						<?php endif; ?>	
					</div>
				</div>
			</div>

			<div class="links">
				<?php 
					$link = get_sub_field('weekly_specials_link');
					if( $link ): 
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
				 ?>

				 	<div class="pickup special">
				 		<a  class="pickup-btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
				 	</div>

				<?php endif; ?>

				<?php if(get_sub_field('pickup_link')): ?>
					<div class="pickup">
						<a href="<?php the_sub_field('pickup_link'); ?>" class="pickup-btn" rel="external">Pickup</a>
					</div>
				<?php endif; ?>

				<?php if(get_sub_field('delivery_link')): ?>
					<div class="pickup">
						<a href="<?php the_sub_field('delivery_link'); ?>" class="pickup-btn" rel="external">Delivery</a>
					</div>
				<?php endif; ?>
	
			</div>

		</div>
	</div>
</article>