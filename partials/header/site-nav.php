<nav class="site-nav">
	<ul class="main-menu">
		<li class="first-level dropdown">
			<a href="#">Restaurants</a>

			<ul class="sub-nav">
				<?php $home = get_option('page_on_front'); if(have_rows('restaurants', $home)): while(have_rows('restaurants', $home)) : the_row(); ?>
				 
				    <?php if( get_row_layout() == 'restaurant' ): ?>
						
						<li class="second-level">
							<a href="#<?php echo sanitize_title_with_dashes(get_sub_field('name')); ?>" class="smooth"><?php the_sub_field('name'); ?></a>
						</li>
						
				    <?php endif; ?>
				 
				<?php endwhile; endif; ?>

			</ul>
		</li>

		<?php if(have_rows('site_nav', 'options')): while(have_rows('site_nav', 'options')): the_row(); ?>

		    <li class="first-level">
		    	<a href="<?php the_sub_field('link'); ?>"<?php if(get_sub_field('external')): ?> rel="external"<?php endif; ?>>
		    		<?php the_sub_field('label'); ?>
	    		</a>
	    	</li>

		<?php endwhile; endif; ?>

	</ul>
</nav>